/*
    This file is part of FCMisc – Florin's C Odds & Ends Library.
    Copyright © 2016,17 Florin Crișan
    Distributed under the MIT license <https://opensource.org/licenses/MIT>
*/

#include "Other.h"

int test(int a, int b) {
    return add(a, b);
}
