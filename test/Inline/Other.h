#ifndef FCMISC_TEST_INLINE_OTHER_H
#define FCMISC_TEST_INLINE_OTHER_H

/*
    This file is part of FCMisc – Florin's C Odds & Ends Library.
    Copyright © 2016,17 Florin Crișan
    Distributed under the MIT license <https://opensource.org/licenses/MIT>
*/

#include <FCMisc.h>

#ifdef __cplusplus
extern "C" {
#endif

FC_INLINE int add(int a, int b) {
    return a + b;
}

int test(int a, int b);

#ifdef __cplusplus
} /*extern "C"*/
#endif

#endif /*FCMISC_TEST_INLINE_OTHER_H*/
