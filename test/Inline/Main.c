/*
    This file is part of FCMisc – Florin's C Odds & Ends Library.
    Copyright © 2016,17 Florin Crișan
    Distributed under the MIT license <https://opensource.org/licenses/MIT>
*/

#include "Other.h"

int main(void) {
    return test(add(1, 2), 3) == 6 ? 0 : 1;
}
