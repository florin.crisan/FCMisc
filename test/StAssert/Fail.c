/*
    This file is part of FCMisc – Florin's C Odds & Ends Library.
    Copyright © 2016,17 Florin Crișan
    Distributed under the MIT license <https://opensource.org/licenses/MIT>
*/

#include <FCMisc.h>

FC_STATIC_ASSERT(sizeof(char) != 1); // Must fail at compile time.

int main(void) {
    return 0;
}
