/*
    This file is part of FCMisc – Florin's C Odds & Ends Library.
    Copyright © 2016,17 Florin Crișan
    Distributed under the MIT license <https://opensource.org/licenses/MIT>
*/

#include <FCMisc/ArSize.h>
#include <FCMisc/StAssert.h>
#include <stdio.h>

static signed char a1[] = {-1};
FC_STATIC_ASSERT(FC_ARRAY_SIZE(a1) == 1);

static unsigned char a2[] = {1, 2};
FC_STATIC_ASSERT(FC_ARRAY_SIZE(a2) == 2);

static const char *a3[] = {"one", "two", "three"};
FC_STATIC_ASSERT(FC_ARRAY_SIZE(a3) == 3);

static int a4[][3] = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}, {10, 11, 12}};
FC_STATIC_ASSERT(FC_ARRAY_SIZE(a4) == 4);

static int a35[35] = {0};
FC_STATIC_ASSERT(FC_ARRAY_SIZE(a35) == 35);

int main(void) {
    (void)a1;
    (void)a2;
    (void)a3;
    (void)a4;
    (void)a35;
    printf(
        "a1 has %u elements\n"
        "a2 has %u elements\n"
        "a3 has %u elements\n"
        "a4 has %u elements\n"
        "a35 has %u elements\n",
        (unsigned int)FC_ARRAY_SIZE(a1),
        (unsigned int)FC_ARRAY_SIZE(a2),
        (unsigned int)FC_ARRAY_SIZE(a3),
        (unsigned int)FC_ARRAY_SIZE(a4),
        (unsigned int)FC_ARRAY_SIZE(a35));
    return 0;
}
