#ifndef FCMISC_STASSERT_H
#define FCMISC_STASSERT_H

/*
    This file is part of FCMisc – Florin's C Odds & Ends Library.
    Copyright © 2016,17 Florin Crișan
    Distributed under the MIT license <https://opensource.org/licenses/MIT>
*/

/**
    @file FCMisc/StaticAssert.h
    @brief Defines the @ref FC_STATIC_ASSERT macro.
*/

/**
    @def FC_STATIC_ASSERT
    @brief A compile time assertion.
*/

#if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 201112L
#define FC_STATIC_ASSERT(x) _Static_assert(x, #x)
#elif defined(__cplusplus) && __cplusplus >= 201103L
#define FC_STATIC_ASSERT(x) static_assert(x, #x)
#else
#define FC_STATIC_ASSERT(x) extern int assertion_failed[(x) ? 1 : -1]
#endif

#endif /*FCMISC_STASSERT_H*/
