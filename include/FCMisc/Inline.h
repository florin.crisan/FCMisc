#ifndef FCMISC_INLINE_H
#define FCMISC_INLINE_H

/*
    This file is part of FCMisc – Florin's C Odds & Ends Library.
    Copyright © 2016,17 Florin Crișan
    Distributed under the MIT license <https://opensource.org/licenses/MIT>
*/

/**
    @file FCMisc/Inline.h
    @brief Defines the @ref FC_INLINE macro.
*/

/**
    @def FC_INLINE
    @brief Declares a function as inline (same as the C++ `inline` keyword).

    C89 doesn't support the `inline` keyword (however, many compilers supported inline functions, through other means).
    C++ uses just `inline` placed before the function definition.
    C99 generally requires `static inline`.
*/

#ifdef __cplusplus
#define FC_INLINE inline
#else
#if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L
#define FC_INLINE static inline
#elif defined(__GNUC__)
#define FC_INLINE static __inline__
#elif defined(_MSC_VER)
#define FC_INLINE __inline
#else
#error Unknown compiler -- please contribute!
#endif
#endif

#endif /*FCMISC_INLINE_H*/
