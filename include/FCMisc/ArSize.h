#ifndef FCMISC_ARSIZE_H
#define FCMISC_ARSIZE_H

/*
    This file is part of FCMisc – Florin's C Odds & Ends Library.
    Copyright © 2016,17 Florin Crișan
    Distributed under the MIT license <https://opensource.org/licenses/MIT>
*/

/**
    @file FCMisc/ArraySize.h
    @brief Defines the @ref FC_ARRAY_SIZE macro.
*/

/**
    @brief Returns the number of elements in an array.

    @warning Do not use on pointers, only on actual arrays.
*/
#define FC_ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))

#endif /*FCMISC_ARSIZE_H*/
