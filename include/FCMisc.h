#ifndef FCMISC_H
#define FCMISC_H

/*
    This file is part of FCMisc – Florin's C Odds & Ends Library.
    Copyright © 2016,17 Florin Crișan
    Distributed under the MIT license <https://opensource.org/licenses/MIT>
*/

#include <FCMisc/ArSize.h>
#include <FCMisc/Inline.h>
#include <FCMisc/StAssert.h>

#endif /*FCMISC_H*/
