/*
    This file is part of FCMisc – Florin's C Odds & Ends Library.
    Copyright © 2016,17 Florin Crișan
    Distributed under the MIT license <https://opensource.org/licenses/MIT>
*/

/*
    Dummy empty file, just so a .lib can be created.
*/

#include <FCMisc.h>

FC_STATIC_ASSERT(sizeof(char) == 1);
